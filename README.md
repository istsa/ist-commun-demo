# Overview

This is an archetype application that can be used to quick start an application based on the [IST web application framework](https://hq.ist.com.gr/svn/YAR/IST/ist-commun/trunk/). 

# How to use this archetype application

> After downloading this application archetype, you must follow the steps below to rename some folders and replace some placeholders in various files with the new values. After that you are ready to start developing your application!

> Refer to the framework documentation for instructions on using the framework

## Rename placeholder names to the actual ones ##

> Tip: You can use a text editor that can search and replace strings in multiple files e.g. [Sublime](http://www.sublimetext.com/) or [Notepad++](https://notepad-plus-plus.org/)

1. Download this archetype using `$ git clone https://bitbucket.org/istsa/ist-commun-demo.git` and providing your *bitucket.org* credentials
2. Delete the `ist-commun-demo\.git` hidden directory (this repository is meant to be a base for cloning and then adding yoyr application to a source control system of your choice)
3. Rename `ist-commun-demo` folder (the base folder of the application) to the new product name
4. Replace `mycompany` to your company name doing a case sensitive search. The new company name should include **only alphanumeric lowercase characters and no spaces**.
5. Replace `myproduct` to the new product name doing a new case sensitive search. The new product name should include **only alphanumeric lowercase characters and no spaces**.
6. Replace `MyCompany` to your company name doing a new case sensitive search.
7. Replace `MyProduct` to the new product name doing a new case sensitive search.

> Note that some folder names and file names should also be renamed during the previous steps. e.g. MyProductConfiguration.java should be renamed

In case your application will be using an oracle database, read [this stackoverflow.com thread](http://stackoverflow.com/questions/1074869/find-oracle-jdbc-driver-in-maven-repository)
that describes the steps to add a dependency to the oracle jdbc driver.

## Optional but recommended: Use vagrant for development

The `Vagrantfile` found in the root directory of the project can be used for easily preparing a development environment. You need to:

1. Install vagrant from [http://www.vagrantup.com/downloads.html](http://www.vagrantup.com/downloads.html) on the development workstation
2. Open a new terminal (command prompt in windows) in the project root directory.
3. Run `$ vagrant up` (and be very patient the first time you do this). This will:
    1. Download an Ubuntu virtual machine with java and maven installed
    2. Install nodejs, npm and grunt
    3. Install PostgreSQL and create default schema and user

## Run the application in development environment

> Note that in the `startREST.sh` script mentioned below it is assumed that there is a `PostgreSQL` installation and there is a `public` schema and a user `postgres` created with the same password. You can modify the scripts to suit your needs.

### Before the first run

1. Open a new terminal (command prompt in windows) in the [modules/webui](modules/webui) directory.
2. Install build dependencies with `npm install` (Retry until command finishes without error)
3. Install runtime dependencies with `bower install`

### Start the REST API

1. Open a terminal (command prompt in windows) in the project root directory.
2. Execute the script `$ ./startREST.sh` (if you are using windows, rename the script to `startREST.cmd`)
3. Wait for the message: `[INFO] Started Jetty Server`

### Start the UI

1. Open a terminal (command prompt in windows) in the project root directory.
2. Execute the script `$ ./startUI.sh` (if you are using windows, rename the script to `startUI.cmd`)
3. Wait for the message: `Running "watch" task \ Waiting...`

### Verify that everything is ok (after the first run)

* Navigate to [localhost:9000](localhost:9000)
* Test that you can log in/out successfully
* Check that the application and company names appear ok in the various parts of the UI
* Test that adding/updating/deleting an entity works ok e.g. create/modify/delete a security role

## Ready to go

You can now add the application in your source control and start developing!