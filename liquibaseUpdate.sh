cd modules/webapp

mvn liquibase:update -Dliquibase.contexts=prod -Ddatabase.username=postgres -Ddatabase.password=postgres -Ddatabase.url=jdbc:postgresql://localhost:15432/postgres -Ddatabase.driverClassName=org.postgresql.Driver -Dspring.profiles.active="postgres,basic-auth" -Dliquibase.verbose=true -Dhibernate.default_schema=public