package gr.mycompany.myproduct;

import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

/**
 * Used for type safe package specification in {@link LocalContainerEntityManagerFactoryBean#setPackagesToScan(String...)}
 * 
 *
 * @version $Rev: 2256 $ $Date: 2013-10-11 17:29:37 +0300 (Παρ, 11 Οκτ 2013) $
 */
public interface MyProductConfiguration {

}
