package gr.mycompany.myproduct.core.repository;


/**
 * Used for type safe package specification in @EnableJpaRepositories
 * 
 *
 * @version $Rev: 2930 $ $Date: 2014-02-20 12:13:37 +0200 (Πεμ, 20 Φεβ 2014) $
 */
public interface RepositoryConfiguration {

}
