package gr.mycompany.myproduct.persistence.jpa.config;

import java.util.UUID;

public interface DbmsSpecificTemplates {
    public String uuidParameterTemplate();
    public String uuidAsSQLParam(UUID uuid);
}
