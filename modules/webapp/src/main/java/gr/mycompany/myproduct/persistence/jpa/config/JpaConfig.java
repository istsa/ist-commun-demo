package gr.mycompany.myproduct.persistence.jpa.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

@Configuration
public interface JpaConfig {
	
	LocalContainerEntityManagerFactoryBean entityManagerFactory();
	DataSource dataSource();
	DbmsSpecificTemplates dbmsSpecificTemplates();
}
