package gr.mycompany.myproduct.persistence.jpa.config;


import gr.com.ist.commun.persistence.jpa.AppRepositoryFactory;
import gr.com.ist.commun.persistence.jpa.AppRepositoryImpl;
import gr.com.ist.commun.persistence.jpa.AppRevisionEntity;
import gr.com.ist.commun.persistence.jpa.AppRevisionEntityInformation;
import gr.com.ist.commun.persistence.jpa.ReflectionRevisionEntityInformation;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.GenericTypeResolver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.core.RepositoryMetadata;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.history.support.RevisionEntityInformation;

/**
 * Special adapter for Springs {@link org.springframework.beans.factory.FactoryBean} interface to allow easy setup of
 * repository factories via Spring configuration.
 * 
 */
public class MyProductRepositoryFactoryBean<R extends JpaRepository<T, I>, T, I extends Serializable>
        extends JpaRepositoryFactoryBean<R, T, I> implements ApplicationContextAware {

    private Class<?> revisionEntityClass;
    private ApplicationContext applicationContext;

    /**
     * Configures the revision entity class.
     * 
     * @param revisionEntityClass
     */
    public void setRevisionEntityClass(Class<?> revisionEntityClass) {
        this.revisionEntityClass = revisionEntityClass;
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean#createRepositoryFactory(javax.persistence.EntityManager)
     */
    @Override
    protected RepositoryFactorySupport createRepositoryFactory(EntityManager entityManager) {
        MyProductRepositoryFactory<T, I> rfs = new MyProductRepositoryFactory<T, I>(entityManager, revisionEntityClass);
        rfs.setApplicationContext(applicationContext);
        return rfs;
    }
    
    /**
     * Repository factory.
     * 
     *
     */
    private static class MyProductRepositoryFactory<T, I extends Serializable> extends JpaRepositoryFactory implements ApplicationContextAware {

        private final RevisionEntityInformation revisionEntityInformation;
        private ApplicationContext applicationContext;
        
        /**
         * Creates a new {@link AppRepositoryFactory} using the given {@link EntityManager} and revision entity class.
         * 
         * @param entityManager must not be {@literal null}.
         * @param revisionEntityClass can be {@literal null}, will default to {@link AppRevisionEntity}.
         */
        public MyProductRepositoryFactory(EntityManager entityManager, Class<?> revisionEntityClass) {
            super(entityManager);
            revisionEntityClass = revisionEntityClass == null ? AppRevisionEntity.class : revisionEntityClass;
            this.revisionEntityInformation = AppRevisionEntity.class.equals(revisionEntityClass) ? new AppRevisionEntityInformation()
                    : new ReflectionRevisionEntityInformation(revisionEntityClass);
            
        }

        /* 
         * (non-Javadoc)
         * @see org.springframework.data.jpa.repository.support.JpaRepositoryFactory#getTargetRepository(org.springframework.data.repository.core.RepositoryMetadata, javax.persistence.EntityManager)
         */
        @Override
        @SuppressWarnings({ "unchecked", "rawtypes" })
        protected <T, ID extends Serializable> SimpleJpaRepository<?, ?> getTargetRepository(RepositoryMetadata metadata,
                EntityManager entityManager) {

            JpaEntityInformation<T, Serializable> entityInformation = (JpaEntityInformation<T, Serializable>) getEntityInformation(metadata
                    .getDomainType());
            // TODO Find a better way to support custom repositories
            return new AppRepositoryImpl(entityInformation, revisionEntityInformation, entityManager);
        }

        @Override
        protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {
            return AppRepositoryImpl.class;
        }
        
        /* (non-Javadoc)
         * @see org.springframework.data.repository.core.support.RepositoryFactorySupport#getRepository(java.lang.Class, java.lang.Object)
         */
        @Override
        public <T> T getRepository(Class<T> repositoryInterface, Object customImplementation) {

            if (RevisionRepository.class.isAssignableFrom(repositoryInterface)) {

                Class<?>[] typeArguments = GenericTypeResolver.resolveTypeArguments(repositoryInterface,
                        RevisionRepository.class);
                Class<?> revisionNumberType = typeArguments[2];

                if (!revisionEntityInformation.getRevisionNumberType().equals(revisionNumberType)) {
                    throw new IllegalStateException(String.format(
                            "Configured a revision entity type of %s with a revision type of %s "
                                    + "but the repository interface is typed to a revision type of %s!", repositoryInterface,
                            revisionEntityInformation.getRevisionNumberType(), revisionNumberType));
                }
            }

            return super.getRepository(repositoryInterface, customImplementation);
        }

        @Override
        public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
            this.applicationContext = applicationContext;
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}