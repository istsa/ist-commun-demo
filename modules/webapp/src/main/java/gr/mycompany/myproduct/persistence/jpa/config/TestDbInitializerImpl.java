package gr.mycompany.myproduct.persistence.jpa.config;


import gr.mycompany.myproduct.web.config.DbInitializer;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

@Configuration
@Profile("data-testFIXME")
public class TestDbInitializerImpl implements DbInitializer{
    
    @Value("classpath:liquibase/initial_data_test.sql")
    private Resource script0;
    
    @Bean
    @Override
    public DataSourceInitializer dataSourceInitializer(DataSource dataSource){
        DataSourceInitializer dataSourceInitializer = new DataSourceInitializer();
        dataSourceInitializer.setDataSource(dataSource);
        ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator();
        resourceDatabasePopulator.addScript(script0);
        resourceDatabasePopulator.setSqlScriptEncoding("UTF-8");
        dataSourceInitializer.setDatabasePopulator(resourceDatabasePopulator);
        return dataSourceInitializer;
    }

}