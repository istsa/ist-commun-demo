package gr.mycompany.myproduct.persistence.jpa.config.hibernate;

import gr.mycompany.myproduct.core.domain.DomainConfiguration;
import gr.mycompany.myproduct.persistence.jpa.config.DbmsSpecificTemplates;
import gr.mycompany.myproduct.persistence.jpa.config.JpaConfig;

import java.util.Properties;
import java.util.UUID;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import com.jolbox.bonecp.BoneCPDataSource;

@Configuration
@Profile("h2")
@PropertySource("classpath:h2.database.properties")
public class H2JpaConfigImpl implements JpaConfig {

    @Autowired
    private Environment env;

    @Bean
    @Override
    @DependsOn("liquibase")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(this.dataSource());
        factoryBean.setPackagesToScan(
                DomainConfiguration.class.getPackage().getName(), 
                gr.com.ist.commun.core.domain.DomainConfiguration.class.getPackage().getName(), 
                gr.com.ist.commun.seqgen.SequenceGenerator.class.getPackage().getName());
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter() {
            {
                this.setDatabase(Database.H2);
            }
        };
        factoryBean.setJpaVendorAdapter(vendorAdapter);
        factoryBean.setJpaProperties(additionalProperties());
        return factoryBean;
    }

    @Bean
    @Override
    public DataSource dataSource() {
        BoneCPDataSource dataSource = new BoneCPDataSource();
        dataSource.setDriverClass(env.getProperty("database.driverClassName"));
        dataSource.setJdbcUrl(env.getProperty("database.url"));
        dataSource.setUsername(env.getProperty("database.username"));
        dataSource.setPassword(env.getProperty("database.password"));
        return dataSource;
    }

    @SuppressWarnings("serial")
    public Properties additionalProperties() {
        return new Properties() {
            {
                setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
                setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
                setProperty("hibernate.ejb.naming_strategy", AppHibernateNamingStrategy.class.getName());
                setProperty("jadira.usertype.autoRegisterUserTypes", "true");
                setProperty("jadira.usertype.databaseZone", "jvm");
                setProperty("jadira.usertype.javaZone", "jvm");
            }
        };
    }

    @Bean
    @Override
    public DbmsSpecificTemplates dbmsSpecificTemplates() {
        return new DbmsSpecificTemplates() {
            @Override
            public String uuidParameterTemplate() {
                return "?";
            }
            @Override
            public String uuidAsSQLParam(UUID uuid) {
                return uuid.toString().replaceAll("-", "").toUpperCase();
            }
        };
    }
}
