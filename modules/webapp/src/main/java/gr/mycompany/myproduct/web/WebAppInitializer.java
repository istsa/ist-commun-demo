package gr.mycompany.myproduct.web;

import gr.mycompany.myproduct.MyProductConfiguration;

import javax.servlet.FilterRegistration;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.h2.server.web.WebServlet;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.filter.RequestContextFilter;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * This class replaces the "old" web.xml and is automatically scanned at the
 * application startup
 */
public class WebAppInitializer implements WebApplicationInitializer {
    private static final String API_DISPATCHER_SERVLET_NAME = "mvc-dispatcher";

    // TODO: Replace this class with one that extends AbstractAnnotationConfigDispatcherServletInitializer
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext rootCtx = new AnnotationConfigWebApplicationContext();
        rootCtx.scan(
                MyProductConfiguration.class.getPackage().getName(), 
                gr.com.ist.commun.Configuration.class.getPackage().getName());
        rootCtx.getEnvironment().setDefaultProfiles("hsql", "data-test", "basic-auth", "liquibase");

        servletContext.addListener(new ContextLoaderListener(rootCtx));

        ServletRegistration.Dynamic apiDispatcher = servletContext.addServlet(API_DISPATCHER_SERVLET_NAME, new DispatcherServlet(rootCtx));
        apiDispatcher.setLoadOnStartup(1);
        apiDispatcher.addMapping("/api", "/api/*");
        // TODO: MultipartConfig should come from configuration
        apiDispatcher.setMultipartConfig(new MultipartConfigElement(System.getProperty("java.io.tmpdir"), 20000000, 20000000, 20000000));

        ////// Localization 
        FilterRegistration.Dynamic localizatonFilter = servletContext.addFilter("localizationFilter", RequestContextFilter.class);
        localizatonFilter.addMappingForUrlPatterns(null, true, "/*");
        
        ////// Hidden http method filter in order to support PATCH operation through AJP v1.3 connectror.
        
        FilterRegistration.Dynamic hiddenHttpMethodFilter = servletContext.addFilter("hiddenHttpMethodFilter", HiddenHttpMethodFilter.class);
        hiddenHttpMethodFilter.addMappingForServletNames(null, true, API_DISPATCHER_SERVLET_NAME);
        
        ////// CORS
        // For parameters checkout https://github.com/ebay/cors-filter
//        FilterRegistration.Dynamic corsFilterRegistration = servletContext.addFilter("corsFilter", CORSFilter.class);
//        corsFilterRegistration.setInitParameter(CORSFilter.PARAM_CORS_ALLOWED_ORIGINS, "http://127.0.0.1:9000,http://localhost:8080,http://localhost:9000,http://localhost:63342,chrome-extension://cokgbflfommojglbmbpenpphppikmonn"); // TODO: externalize
//        corsFilterRegistration.setInitParameter(CORSFilter.PARAM_CORS_ALLOWED_METHODS, "GET,POST,PUT,DELETE,HEAD,OPTIONS"); // TODO: externalize
//        corsFilterRegistration.setInitParameter(CORSFilter.PARAM_CORS_ALLOWED_HEADERS, "Content-Type,X-Requested-With,Accept,Authorization,Origin,Access-Control-Request-Method,Access-Control-Request-Headers,Accept-Language"); // TODO: externalize
//        corsFilterRegistration.setInitParameter(CORSFilter.PARAM_CORS_LOGGING_ENABLED, "true"); // TODO: externalize
//        corsFilterRegistration.addMappingForServletNames(null, true, API_DISPATCHER_SERVLET_NAME);
        
        ////// Security
        // TODO:Remove the following lines when this class is replaced by an extender of AbstractAnnotationConfigDispatcherServletInitializer
        for (String profile : (rootCtx.getEnvironment().getActiveProfiles().length != 0 ? rootCtx.getEnvironment().getActiveProfiles() : rootCtx.getEnvironment().getDefaultProfiles())) {
            if (profile.contains("-auth")) {
                FilterRegistration.Dynamic springSecurityFilterChain = servletContext.addFilter("springSecurityFilterChain", DelegatingFilterProxy.class);
                springSecurityFilterChain.addMappingForUrlPatterns(null, true, "/*");
                break;
            }
        }
        // TODO: Should be activated with special profile
        // Database Console for managing the app's database
        ServletRegistration.Dynamic h2Servlet = servletContext.addServlet("h2console", WebServlet.class);
        h2Servlet.setLoadOnStartup(2);
        h2Servlet.addMapping("/console/database/*");
        //XXX: Must be reviewed for impacts
        FilterRegistration.Dynamic openSessionInViewFilter = servletContext.addFilter("openSessionInViewFilter", OpenEntityManagerInViewFilter.class);
        openSessionInViewFilter.addMappingForUrlPatterns(null, false, "/*");
    }
}
