package gr.mycompany.myproduct.web.config;


import gr.mycompany.myproduct.MyProductConfiguration;
import gr.mycompany.myproduct.persistence.jpa.config.JpaConfig;
import gr.mycompany.myproduct.persistence.jpa.config.MyProductRepositoryFactoryBean;
import liquibase.integration.spring.SpringLiquibase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableSpringConfigured
@EnableJpaRepositories(
        repositoryFactoryBeanClass=MyProductRepositoryFactoryBean.class,
        basePackageClasses = { gr.mycompany.myproduct.core.repository.RepositoryConfiguration.class, 
            gr.com.ist.commun.core.repository.RepositoryConfiguration.class })
// XXX AdviceMode.ASPECTJ is VERY VERY IMPORTANT ATTRIBUTE. Without this @Transactional will not work properly (See http://docs.spring.io/spring/docs/4.0.5.RELEASE/spring-framework-reference/htmlsingle/#transaction)
@EnableTransactionManagement(mode=AdviceMode.ASPECTJ)
public class ApplicationConfig {

    @Autowired
    private JpaConfig jpaConfig;

    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(jpaConfig.entityManagerFactory().getObject());

        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
    
    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(jpaConfig.dataSource());
    }
    
    @Bean
    public AsyncTaskExecutor asyncTaskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(5);
        taskExecutor.setMaxPoolSize(10);
        taskExecutor.setQueueCapacity(25);
        return taskExecutor;
    }

    @Autowired
    private Environment env;

    @Bean(name="liquibase")
    @Profile("liquibase")
    public SpringLiquibase liquibase() {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(jpaConfig.dataSource());
        liquibase.setChangeLog("classpath:" + MyProductConfiguration.class.getPackage().getName().replace('.', '/') + "/master.xml");
        liquibase.setContexts(env.getProperty("liquibase.contexts"));
        return liquibase;
    }

    @Bean(name="liquibase")
    @Profile("!liquibase")
    public SpringLiquibase notLiquibase() {
        return null;
    }

}