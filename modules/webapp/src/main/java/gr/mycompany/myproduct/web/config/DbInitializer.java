package gr.mycompany.myproduct.web.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;

@Configuration
public interface DbInitializer {
	
	DataSourceInitializer dataSourceInitializer(DataSource dataSource);

}
