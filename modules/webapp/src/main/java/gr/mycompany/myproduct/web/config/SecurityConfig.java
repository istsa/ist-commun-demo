package gr.mycompany.myproduct.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Profile;
import org.springframework.web.filter.DelegatingFilterProxy;

@Configuration
@ImportResource({ "classpath:META-INF/spring/security.xml" })
@Profile({ "basic-auth", "digest-md5-auth", "cas-sso-auth" })
public class SecurityConfig {

    @Bean
    public DelegatingFilterProxy springSecurityFilterChain() {
        DelegatingFilterProxy filterProxy = new DelegatingFilterProxy();
        return filterProxy;
    }
}