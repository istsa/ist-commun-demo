package gr.mycompany.myproduct.web.config;

import gr.com.ist.commun.core.domain.security.audit.entity.PropertyChangeAuditJacksonFilter;
import gr.com.ist.commun.core.validation.CompositeValidator;
import gr.com.ist.commun.web.view.ContentNegotiatingAwareJasperReportsViewResolver;
import gr.com.ist.commun.web.view.ContentNegotiatingJasperReportsView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.rest.webmvc.json.Jackson2DatatypeHelper;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.RequestToViewNameTranslator;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.BeanNameViewResolver;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.ResourceBundleViewResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;
import com.fasterxml.jackson.datatype.joda.JodaModule;

@Configuration
@EnableWebMvc
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        Hibernate4Module hbMod = new Hibernate4Module();
        hbMod.configure(Hibernate4Module.Feature.FORCE_LAZY_LOADING, true);

        FilterProvider filters = new SimpleFilterProvider().addFilter("collectionModificationFilter", new PropertyChangeAuditJacksonFilter());
        
        ObjectMapper jsonMapper = new ObjectMapper();
        jsonMapper.registerModule(hbMod);
        jsonMapper.setFilters(filters);
        jsonMapper.registerModule(new JodaModule());
        
        MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
        jsonConverter.setObjectMapper(jsonMapper);
        
        converters.add(jsonConverter);
        converters.add(new StringHttpMessageConverter());
        super.configureMessageConverters(converters);
    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
      configurer.enable();
    }
    
    
    
// TODO: Remove mime types from web.xml and add them below (See also http://docs.spring.io/spring/docs/3.2.4.RELEASE/spring-framework-reference/html/mvc.html#mvc-config-content-negotiation to configureContentNegotiation)
//    @Override
//    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
//      configurer.mediaType("json", MediaType.parseMediaType("application/json"));
//      configurer.mediaType("xml", MediaType.parseMediaType("application/xml"));
//      configurer.mediaType("pdf", MediaType.parseMediaType("application/pdf"));
//      configurer.mediaType("txt", MediaType.parseMediaType("text/plain"));
//      configurer.mediaType("csv", MediaType.parseMediaType("text/csv"));
//      configurer.mediaType("docx", MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.wordprocessingml.document"));
//      configurer.mediaType("pptx", MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.presentationml.presentation"));
//      configurer.mediaType("xls", MediaType.parseMediaType("application/vnd.ms-excel"));
//    }

    /**
     * Used to avoid specifying the name of the views in controllers.
     * Declared here just to show explicitly that the application depends on 
     * the functionality.
     */
    @Bean
    public RequestToViewNameTranslator viewNameTranslator() {
        return new org.springframework.web.servlet.view.DefaultRequestToViewNameTranslator();
    }
    
    @Bean
    public ContentNegotiatingViewResolver contentNegotiatingViewResolver() {

        ContentNegotiatingViewResolver contentNegotiatingViewResolver = new ContentNegotiatingViewResolver();
        contentNegotiatingViewResolver.setUseNotAcceptableStatusCode(true);
        
        List<ViewResolver> viewResolvers = new ArrayList<ViewResolver>();

        viewResolvers.add(new ResourceBundleViewResolver());
        //viewResolvers.add(new XmlViewResolver());
        viewResolvers.add(new BeanNameViewResolver());

        ContentNegotiatingAwareJasperReportsViewResolver jasperReportsViewResolver = jasperReportsViewResolver();
        viewResolvers.add(jasperReportsViewResolver);
        
        InternalResourceViewResolver internalResourceViewResolver = internalResourceViewResolver();
        viewResolvers.add(internalResourceViewResolver);
        
        contentNegotiatingViewResolver.setViewResolvers(viewResolvers);

        List<View> defaultViews = new LinkedList<View>();
        defaultViews.add(mappingJackson2JsonView());
        defaultViews.add(mappingJackkson2XmlView());

        contentNegotiatingViewResolver.setDefaultViews(defaultViews);
        
        return contentNegotiatingViewResolver;
    }

    @Bean
    public InternalResourceViewResolver internalResourceViewResolver() {
        InternalResourceViewResolver internalResourceViewResolver = new InternalResourceViewResolver();
        return internalResourceViewResolver;
    }

    @Bean
    public ContentNegotiatingAwareJasperReportsViewResolver jasperReportsViewResolver() {
        ContentNegotiatingAwareJasperReportsViewResolver jasperReportsViewResolver = new ContentNegotiatingAwareJasperReportsViewResolver();
        jasperReportsViewResolver.setViewClass(ContentNegotiatingJasperReportsView.class);
        jasperReportsViewResolver.setPrefix("classpath:jasper/");
        jasperReportsViewResolver.setSuffix(".jasper");
        return jasperReportsViewResolver;
    }

    @Bean
    public MappingJackson2JsonView mappingJackkson2XmlView() {
        MappingJackson2JsonView mappingJackson2JXmlView = new MappingJackson2JsonView();
        mappingJackson2JXmlView.setContentType("application/xml");
        
        JacksonXmlModule xmlModule = new JacksonXmlModule();
        xmlModule.setDefaultUseWrapper(false);
        ObjectMapper objectMapper = new XmlMapper(xmlModule);
        Jackson2DatatypeHelper.configureObjectMapper(objectMapper);
        AnnotationIntrospector introspector = new JacksonAnnotationIntrospector();
        objectMapper.setAnnotationIntrospector(introspector);
        objectMapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, false);
        mappingJackson2JXmlView.setObjectMapper(objectMapper);
        
        return mappingJackson2JXmlView;
    }

    @Bean
    public MappingJackson2JsonView mappingJackson2JsonView() {
        MappingJackson2JsonView mappingJackson2JJsonView = new MappingJackson2JsonView();
        Jackson2DatatypeHelper.configureObjectMapper(mappingJackson2JJsonView.getObjectMapper());
//        mappingJackson2JJsonView.getObjectMapper().configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        return mappingJackson2JJsonView;
    }

    @Bean
    public StandardServletMultipartResolver multipartResolver() {
        return new StandardServletMultipartResolver();
    }
    
    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames(
                "classpath:org/springframework/security/messages",
                "classpath:gr/hcaa/avis/core/domain/messages",
                "classpath:gr/com/ist/commun/core/domain/security/messages",
                "classpath:rest-messages"
        );
        messageSource.setDefaultEncoding("UTF-8");
        messageSource.setUseCodeAsDefaultMessage(true);
        messageSource.setFallbackToSystemLocale(false);
        return messageSource;
    }
    
    @Bean
    CompositeValidator validator() {
        CompositeValidator validator = new CompositeValidator();
        validator.setValidationMessageSource(messageSource());
        return validator;
    }
    
    /**
     * Add validators in composite validators to be used from WebMvc (including REST API)
     */
    @Override
    public Validator getValidator() {
        return validator();
    }

}
