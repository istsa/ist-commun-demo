package gr.mycompany.myproduct.web.config;

import gr.com.ist.commun.support.CsvImporter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.core.mapping.ResourceDescription;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.data.rest.webmvc.json.PersistentEntityToJsonSchemaConverter;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.http.converter.HttpMessageConverter;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
public class WebMvcRestConfig extends RepositoryRestMvcConfiguration {

    @Autowired
    WebMvcConfig webMvcConfig;
    
    @Override
    protected void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener validatingListener) {
        validatingListener.addValidator("beforeCreate", webMvcConfig.getValidator());
        validatingListener.addValidator("beforeSave", webMvcConfig.getValidator());
    }

    @Override
    protected void configureJacksonObjectMapper(ObjectMapper objectMapper) {
        super.configureJacksonObjectMapper(objectMapper);
        // XXX We use this flag in order for getAuthorities() in User to work correctly.
        objectMapper.configure(MapperFeature.USE_GETTERS_AS_SETTERS, false);
    }
    
    /**
     * Turns a domain class into a {@link org.springframework.data.rest.webmvc.json.JsonSchema}.
     * 
     * @return
     */
    @Bean
    public PersistentEntityToJsonSchemaConverter jsonSchemaConverter() {
        /*
         * TODO Enhance PersistentEntityToJsonSchemaConverter to return all
         * information necessary for javascript UI to be and application viewer,
         * i.e. have no application business logic
         */
        return super.jsonSchemaConverter();
    }
        
    
    /**
     * The {@link MessageSourceAccessor} to provide messages for {@link ResourceDescription}s being rendered.
     * 
     * @return
     */
    @Bean
    public MessageSourceAccessor resourceDescriptionMessageSourceAccessor() {
        return new MessageSourceAccessor(webMvcConfig.messageSource());
    }

    @Override
    protected void configureHttpMessageConverters(List<HttpMessageConverter<?>> messageConverters) {
        super.configureHttpMessageConverters(messageConverters);
    }

    @Bean
    public CsvImporter csvImporter() {
        return new CsvImporter(csvConversionService(), repositories(), resourceMappings());
    }
    
    @Bean
    public FormattingConversionService csvConversionService() {

        // Use the DefaultFormattingConversionService and register defaults
        DefaultFormattingConversionService conversionService = new DefaultFormattingConversionService();

//        // Ensure @NumberFormat is still supported
//        conversionService.addFormatterForFieldAnnotation(new NumberFormatAnnotationFormatterFactory());

//        // Register date conversion with a specific global format
//        DateFormatterRegistrar registrar = new DateFormatterRegistrar();
//        DateFormatter dateFormatter = new DateFormatter("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
//        registrar.setFormatter(dateFormatter);
//        registrar.registerFormatters(conversionService);

        return conversionService;
    }

    @Override
    protected void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        super.configureRepositoryRestConfiguration(config);
        // TODO configure via value
        config.setMaxPageSize(100000);
    }

}