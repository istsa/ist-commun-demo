package gr.mycompany.myproduct.web.controller;

import gr.com.ist.commun.core.domain.security.EntityBase;
import gr.com.ist.commun.core.repository.SearchableByTerm;
import gr.com.ist.commun.core.validation.ValidationErrorsException;
import gr.com.ist.commun.persistence.jpa.AppRepository;
import gr.com.ist.commun.support.CsvImporter;

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.support.Repositories;
import org.springframework.data.rest.core.mapping.ResourceMappings;
import org.springframework.data.rest.core.mapping.ResourceMetadata;
import org.springframework.data.rest.webmvc.ControllerUtils;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.supercsv.prefs.CsvPreference;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;


/**
 * TODO: Support limiting the fields contained in the response (query projections)
 * TODO: Support query by example
 *
 */
@org.springframework.web.bind.annotation.RestController
public class RestController {
    private Map<String, Object> pathToRepoMap = 
            new HashMap<String, Object>();
    
    @SuppressWarnings("unused")
	private static Logger LOG = LoggerFactory.getLogger(RestController.class);
    
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }
    
    @Autowired
    public RestController(
            Repositories repositories,
            ResourceMappings resourceMappings) {
        for (ResourceMetadata resourceMetadata : resourceMappings) {
            this.pathToRepoMap.put(resourceMetadata.getPath().toString().substring(1), repositories.getRepositoryFor(resourceMetadata.getDomainType()));
        }
    }

    /**
     * A resource which exposes search using keywords for repositories that support it. 
     * {@link Pageable} argument is resolved through infrastructure
     * components registered through {@link EnableSpringDataWebSupport} on
     * {@link RepositoryRestMvcConfiguration}.
     * 
     * @param pageable the {@link Pageable} derived from the request.
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value="/{repositoryName}/query", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<Page<Object>> searchByKeyword(@PathVariable String repositoryName, Pageable pageable, @RequestParam(value="q", required=false) String term, @RequestParam(value="fields", required=false) String fields) {
        Object repository = this.pathToRepoMap.get(repositoryName);
        if (repository == null) {
            return new ResponseEntity<Page<Object>>(HttpStatus.NOT_FOUND);
        }
        Page<Object> page;
        if (repository instanceof SearchableByTerm<?>) {
            SearchableByTerm<Object> searchableRepo = (SearchableByTerm<Object>) repository;

            page = searchableRepo.searchTerm(term, pageable);
        } else {
            JpaRepository<?, ?> repo = (JpaRepository<?, ?>) repository;
            page = (Page<Object>) repo.findAll(pageable);
        }
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    @Autowired
    @Qualifier("defaultConversionService")
    private ConversionService conversionService;
    @Autowired
    private CsvImporter csvImporter;
    @Autowired
    private Validator validator;

    @Autowired
    @Qualifier("jacksonHttpMessageConverter")
    private MappingJackson2HttpMessageConverter converter;
    
    // TODO Support sending a nested object containing query by example instead of id for referenced entities
    @RequestMapping(value = "/{repositoryName}/bulk", method = {RequestMethod.POST, RequestMethod.PUT, RequestMethod.PATCH}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ResourceSupport> bulkImportJson(@PathVariable String repositoryName, Reader inputReader, HttpMethod httpMethod) {
        @SuppressWarnings("unchecked")
        AppRepository<EntityBase, Serializable, ?> repository = (AppRepository<EntityBase, Serializable, ?>) this.pathToRepoMap.get(repositoryName);
        if (repository == null) {
            return ControllerUtils.toEmptyResponse(HttpStatus.NOT_FOUND);
        }

        ArrayList<EntityBase> targetList = new ArrayList<>();
        class ResourcesBean<T> {
            private List<T> content;
            public ResourcesBean(List<T> content) {
                this.content = content;
            }
            @SuppressWarnings("unused")
            public List<T> getContent() {
                return this.content;
            }
        }
        ResourcesBean<EntityBase> resources = new ResourcesBean<>(targetList);
        BeanPropertyBindingResult errors = new BeanPropertyBindingResult(resources, repositoryName);
        try {
            ObjectMapper mapper = converter.getObjectMapper();
            ObjectNode rootNode = (ObjectNode) mapper.readTree(inputReader);
            ArrayNode contentNode = (ArrayNode) rootNode.get("content");
            for (JsonNode jsonNode : contentNode) {
                JsonNode idNode = jsonNode.get("id");
                ObjectReader reader = null;
                Object existingObject = null;
                if (idNode != null && httpMethod.equals(HttpMethod.PATCH)) {
                    existingObject = conversionService.convert(idNode.asText(), repository.getEntityInformation().getJavaType());
                }
                if (existingObject != null) {
                    reader = mapper.readerForUpdating(existingObject);
                } else {
                    reader = mapper.reader(repository.getEntityInformation().getJavaType());
                }
                EntityBase targetEntity = null;
                targetEntity = reader.readValue(jsonNode);
                if (httpMethod.equals(HttpMethod.POST)) {
                    // XXX Trick to trigger persist instead of merge in Hibernate
                    targetEntity.setVersion(null);
                }
                targetList.add(targetEntity);
                errors.pushNestedPath("content["+(targetList.size() - 1)+"]");
                validator.validate(targetEntity, errors);
                if ((idNode == null || existingObject == null) && httpMethod.equals(HttpMethod.PATCH)) {
                    errors.rejectValue("id", "notFound");
                }
                errors.popNestedPath();
            }
            if (errors.hasErrors()) {
                throw new ValidationErrorsException(errors, resources);
            }
            repository.save(targetList);
            return ControllerUtils.toEmptyResponse(HttpStatus.NO_CONTENT);
        } catch (ObjectOptimisticLockingFailureException e) {
            for (int i=0; i<targetList.size(); i++) {
                EntityBase entity = targetList.get(i);
                if (entity.getId() != null && entity.getId().equals(e.getIdentifier())) {
                    errors.rejectValue("content["+i+"].version", "optimisticLockingFailure", new Object[] {entity.getId(), entity.getVersion()}, "Δεν βρέθηκε εγγραφή με id "+entity.getId()+" και έκδοση "+entity.getVersion());
                    break;
                }
            }
            throw new ValidationErrorsException(errors, resources);
        } catch (JsonProcessingException e) {
            throw new HttpMessageNotReadableException(e.getMessage(), e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private final String TSV = "text/tab-separated-values";
    @RequestMapping(value = "/{repositoryName}/bulk", method = {RequestMethod.POST, RequestMethod.PUT, RequestMethod.PATCH}, consumes = {"text/csv", TSV})
    public ResponseEntity<ResourceSupport> bulkImportCsv(@PathVariable String repositoryName, Reader contents, @RequestHeader(value="Content-Type") MediaType mediaType, HttpMethod httpMethod) {
        if (contents != null) {
            @SuppressWarnings("unchecked")
            AppRepository<Object, Serializable, ?> repository = (AppRepository<Object, Serializable, ?>) this.pathToRepoMap.get(repositoryName);
            if (repository == null) {
                return ControllerUtils.toEmptyResponse(HttpStatus.NOT_FOUND);
            }

            char delimiterChar = ',';
            String delimiterParam;
            if (TSV.equals(mediaType.getType())) {
                delimiterChar = '\t';
            } else if ((delimiterParam = mediaType.getParameter("delimiter")) != null) {
                delimiterChar = delimiterParam.matches("0x[0-9a-fA-F]+") ?
                        (char) Integer.parseInt(delimiterParam.substring(2), 16) :
                        delimiterParam.charAt(0);
            }
            CsvPreference csvPreference = new CsvPreference.Builder('"', delimiterChar, "\r\n").build();

            List<Object> list = csvImporter.readDomainObjects(repository, contents, csvPreference, httpMethod.equals(HttpMethod.PATCH), httpMethod.equals(HttpMethod.PUT), validator);
            repository.save(list);
        }

        return ControllerUtils.toEmptyResponse(HttpStatus.NO_CONTENT);
    }

    /**
     * {@code GET / repository}/{id}}
     * 
     * @param repoRequest
     * @param id
     * @return
     * @throws ResourceNotFoundException
     */
    @ResponseBody
    @RequestMapping(value = "/{repositoryName}/query/{id}", method = RequestMethod.GET, produces = {
            "application/json" })
    public ResponseEntity<?> getSingleEntity(@PathVariable String repositoryName, @PathVariable java.util.UUID id) {

        @SuppressWarnings("rawtypes")
        CrudRepository repo = (CrudRepository) this.pathToRepoMap.get(repositoryName);

        if (repo == null) {
            return new ResponseEntity<PagedResources<Resource<Object>>>(HttpStatus.NOT_FOUND);
        }

        @SuppressWarnings("unchecked")
        Object domainObj = repo.findOne(id);

        if (domainObj == null) {
            return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Object>(domainObj, HttpStatus.OK);
    }

    @PersistenceContext
    private EntityManager em;

    @RequestMapping(value="/jpql", produces = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<?> jpql(Pageable pageable, @RequestParam(value="q", required=true) String jpqlQuery) {
        if (jpqlQuery == null || !jpqlQuery.toLowerCase().trim().startsWith("select ")) {
            throw new IllegalArgumentException("Required string parameter 'q' should start with 'select '");
        }
        Query query = em.createQuery(jpqlQuery);
        query.setFirstResult(pageable.getOffset());
        query.setMaxResults(pageable.getPageSize());
        return new ResponseEntity<>(query.getResultList(), HttpStatus.OK);
    }

}
