package org.springframework.data.rest.webmvc.support;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.rest.core.RepositoryConstraintViolationException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Jon Brisbin
 * 
 * Patched by apostolakis.antonis@gmail.com on 2014/10/03 to handle global errors
 */
public class RepositoryConstraintViolationExceptionMessage {

	private final List<ValidationError> errors = new ArrayList<ValidationError>();

	public RepositoryConstraintViolationExceptionMessage(RepositoryConstraintViolationException violationException,
			MessageSourceAccessor accessor) {

		List<ObjectError> validationErrors = violationException.getErrors().getAllErrors();
		Locale currentLocale =  LocaleContextHolder.getLocale();

		for (ObjectError error: validationErrors) {
			FieldError fieldError = error instanceof FieldError ? (FieldError) error : null;
			List<Object> args = new ArrayList<Object>();
			args.add(error.getObjectName());
			args.add(fieldError == null ? null : fieldError.getField());
			args.add(fieldError == null ? null : fieldError.getRejectedValue());
			if (null != error.getArguments()) {
				for (Object o : error.getArguments()) {
					args.add(o);
				}
			}
			String localizedErrorMessage = accessor.getMessage(error.getCode(), args.toArray(), error.getDefaultMessage(), currentLocale);
			if (fieldError != null) {
				this.errors.add(new ValidationError(error.getObjectName(), localizedErrorMessage, String.format("%s",
						fieldError.getRejectedValue()), fieldError.getField()));
				
			} else {
				this.errors.add(new ValidationError(error.getObjectName(), localizedErrorMessage, null, null));
			}
		}
		
	}

	@JsonProperty("errors")
	public List<ValidationError> getErrors() {
		return errors;
	}

	public static class ValidationError {

		private final String entity;
		private final String message;
		private final String invalidValue;
		private final String property;

		public ValidationError(String entity, String message, String invalidValue, String property) {
			this.entity = entity;
			this.message = message;
			this.invalidValue = invalidValue;
			this.property = property;
		}

		public String getEntity() {
			return entity;
		}

		public String getMessage() {
			return message;
		}

		public String getInvalidValue() {
			return invalidValue;
		}

		public String getProperty() {
			return property;
		}
	}

}
