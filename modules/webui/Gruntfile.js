'use strict';

// For help on task configuration see http://gruntjs.com/configuring-tasks
	
// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    yeoman: {
      // configurable paths
      app: require('./bower.json').appPath || 'app',
      dist: 'dist'
    },

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      js: {
        files: ['{.tmp,<%= yeoman.app %>}/modules/**/*.js'],
        tasks: ['newer:jshint:all']
      },
      jsTest: {
        files: ['test/spec/{,*/}*.js'],
        tasks: ['newer:jshint:test', 'karma']
      },
      styles: {
        files: ['<%= yeoman.app %>/styles/{,*/}*.css', '<%= yeoman.app %>/de-metr/{,*/}*.css'],
        tasks: ['newer:copy:styles', 'autoprefixer']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= yeoman.app %>/{,*/}*.html',
          '.tmp/styles/{,*/}*.css',
          '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg,ico}',
          '{.tmp,<%= yeoman.app %>}/modules/**/*.js',
          ['{.tmp,<%= yeoman.app %>}/modules/**/*.html']
        ]
      }
    },

    // The actual grunt server settings
    connect: {
      options: {
        port: 9000,
        // Change this to '0.0.0.0' to access the server from outside.
        hostname: '0.0.0.0',
        livereload: 35729
      },
      proxies: [{
        context: '/api',
        host: 'localhost',
        port: 8080,
        https: false,
        changeOrigin: false,
        xforward: false
      }, {
        context: '/login',
        host: 'localhost',
        port: 8080,
        https: false,
        changeOrigin: false,
        xforward: false
      }, {
        context: '/logout',
        host: 'localhost',
        port: 8080,
        https: false,
        changeOrigin: false,
        xforward: false
      }],
      livereload: {
        options: {
          open: true,
          base: [
            '.tmp',
            '<%= yeoman.app %>'
          ],
          middleware: function (connect, options) {
            var middlewares = [];
            var directory = options.directory || options.base[options.base.length - 1];
            if (!Array.isArray(options.base)) {
              options.base = [options.base];
            }
            options.base.forEach(function(base) {
              // Serve static files.
              middlewares.push(connect.static(base));
            });

            // Setup the proxy
            middlewares.push(require('grunt-connect-proxy/lib/utils').proxyRequest);

            // Make directory browse-able.
            middlewares.push(connect.directory(directory));

            return middlewares;
          }
        }
      },
      test: {
        options: {
          port: 7000,
          base: [
            '.tmp',
            'test',
            '<%= yeoman.app %>'
          ]
        }
      },
      dist: {
        options: {
          base: '<%= yeoman.dist %>'
        }
      }
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: [
        'Gruntfile.js',
        '<%= yeoman.app %>/modules/{,*/}*.js'
      ],
      test: {
        options: {
          jshintrc: 'test/.jshintrc'
        },
        src: ['test/spec/{,*/}*.js']
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= yeoman.dist %>/*',
            '!<%= yeoman.dist %>/.git*'
          ]
        }]
      },
      server: '.tmp',
      tmp: '.tmp'
    },

    // Add vendor prefixed styles
    autoprefixer: {
      options: {
        browsers: ['last 1 version']
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      }
    },

    // Renames files for browser caching purposes
    rev: {
      dist: {
        files: {
          src: [
//          '<%= yeoman.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg,ico}',
            '<%= yeoman.dist %>/images/*.{png,jpg,jpeg,gif,webp,svg,ico}'
          ]
        }
      },
      postRequire: {
        files: {
          src: [
            '<%= yeoman.dist %>/modules/{,*/}*.js',
            '<%= yeoman.dist %>/styles/{,*/}*.css',
          ]
        }
      }
    },

    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      html: '<%= yeoman.app %>/index.html',
      options: {
        dest: '<%= yeoman.dist %>'
      }
    },

    // Performs rewrites based on rev and the useminPrepare configuration
    usemin: {
      html: ['<%= yeoman.dist %>/{,**/}*.html'],
      css: ['<%= yeoman.dist %>/styles/{,*/}*.css'],
      options: {
        assetsDirs: ['<%= yeoman.dist %>']
      }
    },

    // The following *-min tasks produce minified files in the dist folder
    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: '{,*/}*.{png,jpg,jpeg,gif,ico}',
          dest: '<%= yeoman.dist %>/images'
        }]
      }
    },
    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: '{,*/}*.svg',
          dest: '<%= yeoman.dist %>/images'
        }]
      }
    },
    htmlmin: {
      dist: {
        options: {
          // Optional configurations that you can uncomment to use
          // removeCommentsFromCDATA: true,
          // collapseBooleanAttributes: true,
          // removeAttributeQuotes: true,
          // removeRedundantAttributes: true,
          // useShortDoctype: true,
          // removeEmptyAttributes: true,
          // removeOptionalTags: true*/
        },
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>',
          src: ['*.html', 'views/*.html'],
          dest: '<%= yeoman.dist %>'
        }]
      }
    },

    // Allow the use of non-minsafe AngularJS files. Automatically makes it
    // minsafe compatible so Uglify does not destroy the ng references
    ngmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/concat/scripts',
          src: '*.js',
          dest: '.tmp/concat/scripts'
        }]
      }
    },

    // Replace Google CDN references
    cdnify: {
      dist: {
        html: ['<%= yeoman.dist %>/*.html']
      }
    },

    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [
          {
            expand: true,
            dot: true,
            cwd: '<%= yeoman.app %>',
            dest: '<%= yeoman.dist %>',
            src: [
              '*.{ico,png,txt}',
              '.htaccess',
              'bower_components/{,**/}*',
              'modules/{,**/}*',
              'help/{,**/}*',
              'images/{,*/}*.{webp}',
              'styles{,**/}*'
            ]
          },
          {
            expand: true,
            cwd: '.tmp/images',
            dest: '<%= yeoman.dist %>/images',
            src: [
              'generated/*'
            ]
          }
        ]
      },
      styles: {
        expand: true,
        cwd: '<%= yeoman.app %>/styles',
        dest: '.tmp/styles/',
        src: '{,*/}*.css'
      },
      tmp: {
        expand: true,
        cwd: '<%= yeoman.dist %>',
        dest: '.tmp',
        src: '{,**/}*'
      }
    },

    // Run some tasks in parallel to speed up the build process
    concurrent: {
      server: [
        'copy:styles'
      ],
      test: [
        'copy:styles'
      ],
      dist: [
        'copy:styles',
        'imagemin',
        'svgmin',
        'htmlmin'
      ]
    },

    // By default, your `index.html`'s <!-- Usemin block --> will take care of
    // minification. These next options are pre-configured if you do not wish
    // to use the Usemin blocks.
    // cssmin: {
    //   dist: {
    //     files: {
    //       '<%= yeoman.dist %>/styles/main.css': [
    //         '.tmp/styles/{,*/}*.css',
    //         '<%= yeoman.app %>/styles/{,*/}*.css'
    //       ]
    //     }
    //   }
    // },
    // uglify: {
    //   dist: {
    //     files: {
    //       '<%= yeoman.dist %>/scripts/scripts.js': [
    //         '<%= yeoman.dist %>/scripts/scripts.js'
    //       ]
    //     }
    //   }
    // },
    // concat: {
    //   dist: {}
    // },

    // Test settings
    karma: {
      unit: {
        configFile: 'karma.conf.js',
        singleRun: true
      }
    },

    requirejs: {
      dist: {
        options: {
          // *insert almond in all your modules
//          almond: true,
          // *replace require script calls, with the almond modules
          // in the following files
//          replaceRequireScript: [{
//            files: ['<%= yeoman.dist %>/index.html'],
//            module: 'modules/main'
//          }],
          // "normal" require config
          // *create at least a 'main' module
          // thats necessary for using the almond auto insertion
            
          // For description of each of the following options ...
          // ... see: https://github.com/jrburke/r.js/blob/master/build/example.build.js
          baseUrl: 'modules',
          mainConfigFile: [
            '.tmp/bower_components/ist-commun-webui/app/modules/requirejs-config.js',
            '.tmp/modules/requirejs-config.js'
          ],
          optimizeCss: 'standard', // none | standard
          appDir: '.tmp',
          dir: '<%= yeoman.dist %>',
//          out: '<%= yeoman.dist %>/modules/main.js',
//          fileExclusionRegExp: /^\./, // TODO: do not copy files that are included in the optimized build
          fileExclusionRegExp: /^\.|(?:.+\.min\.js(?:\..+)?)/, // TODO: do not copy files that are included in the optimized build
          skipDirOptimize: true,
          locale: 'el-gr',
//          generateSourceMaps: true,
//          useSourceUrl: true,
          preserveLicenseComments: false, // TODO: work out how best to surface the license information
          removeCombined: true,
          optimize: 'none', // uglify2 | none
          waitSeconds: 15,
//          siteRoot: '.', // See https://github.com/guybedford/require-css#siteroot-configuration
          separateCSS: false, // output the CSS to a separate file
          buildCSS: true,
          modules: [
            {
              name: 'main',
              include: ['app'],
              exclude: ['require-css/normalize'] // As suggested in https://github.com/guybedford/require-css#basic-usage
            }
          ]
//          onBuildRead: function (moduleName, path, contents) {
//            var ngmin = require('ngmin');
//            var ngmined = ngmin.annotate(contents);
//            if (ngmined !== contents) {
//              console.log('ngmin modified: '+moduleName);
//            }
//            return ngmined;
//          }
        }
      }
    },
       
//    'cache-busting': {
//      requirejs: {
//        replace: ['<%= yeoman.dist %>/index.html'],
//        replacement: 'main.js',
//        file: '<%= yeoman.dist %>/modules/main.js'
//      },
//    },
    license: {
      options: {
        unknown: true,
        start: '.',
        depth: null,
        output: 'file'
      }
    },

  });


  grunt.registerTask('serve', function (target) {
    if (target === 'dist') {
      return grunt.task.run(['build', 'connect:dist:keepalive']);
    }

    grunt.task.run([
      'clean:server',
      'concurrent:server',
      'autoprefixer',
      'configureProxies',
      'connect:livereload',
      'watch'
    ]);
  });

  grunt.registerTask('server', function () {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run(['serve']);
  });

  grunt.registerTask('test', [
    'clean:server',
    'concurrent:test',
    'autoprefixer',
    'connect:test',
//    'karma'
  ]);

  grunt.registerTask('build', [
    'clean:dist',
    'useminPrepare',
    'concurrent:dist',
    'autoprefixer',
    // 'concat',
    'ngmin',
//    'cache-busting',
    'copy:dist',
    'cdnify',
    // 'cssmin',
//    'uglify',
    'rev:dist',
    'usemin',
    'clean:tmp',
    'copy:tmp',
    'requirejs:dist',
    'rev:postRequire',
    'usemin',
  ]);

  grunt.registerTask('license', 'List all packages (and their sub-packages) that this project depends on with license information', function() {
    function convertToCsv(data) {
      var ret = '', module, licenses, repository;

      for (module in data) {
        if (data.hasOwnProperty(module)) {
          licenses = data[module].licenses || '';
          repository = data[module].repository || '';
          ret = ret + module + ';' + licenses + ';' + repository + '\r\n';
        }
      }

      return ret;
    }
    var checker = require('license-checker'),
      fs = require('fs'),
      done = this.async(),
      defaults = {
        start: '.',
        unknown: false,
        depth: 1,
        include: 'all',
        output: 'console', //console or file
        filename: 'LICENSES',
        format: 'json' //json or csv
      },
      options = grunt.util._.extend(defaults, this.options());

    checker.init(options, function(data){
      if (options.format === 'csv') {
        data = convertToCsv(data);
      } else {
        data = JSON.stringify(data, null, 4);
      }

      if (options.output === 'file') {
        fs.writeFile(options.filename, data, function() {
          console.log('Successfully written '.green + options.filename.grey);
          done();
        });
      } else if (options.output === 'console') {
        grunt.log.writeln(data);
      } else {
        grunt.log.writeln('Unknown output channel: ' + options.output);
      }
    });
  });
  
  grunt.registerTask('default', [
    'newer:jshint',
    //'test', // TODO Enable 'test' when we add some tests and find out how to run them on jenkins
    'build'
  ]);

};
