# Getting started

## Installation

Make sure you have installed Node.js.
Then install the required tools globally by running:

`npm install -g yo`

This will install Grunt and Bower automatically.

## Setting up the project

After checking out or updating from SCM:

* Change to the project's root directory.
* Install project dependencies with `npm install`.
* Install bower packages with `bower install`.

That's really all there is to it. Installed Grunt tasks can be listed by running `grunt --help`.

## Usage

A complete workflow might look like this:

    bower install underscore --save # install a dependency for your project from Bower
    grunt serve                     # preview your app
    grunt test                      # test your app
    grunt                           # build the application for deployment
    grunt license                   # list all packages and their licenses this project depends on

## Warning

To avoid broken minified build you should make sure that:
* Use the inline bracket notation which wraps the function to be injected into an array of strings (representing the dependency names) followed by the function to be injected.
* Do not use root / in URLs

## References

### Build tools
* <http://yeoman.io>
* <http://gruntjs.com/getting-started>
* <http://bower.io>

### RequireJS, Angular
* <http://angular-ui.github.io/>
* <http://kendo-labs.github.io/angular-kendo/#/>

* <https://github.com/amdjs/amdjs-api/wiki/AMD>
* <http://nikku.github.io/requirejs-angular-define/>
* <http://weblogs.asp.net/dwahlin/archive/2013/08/16/using-an-angularjs-factory-to-interact-with-a-restful-service.aspx>
* <http://fastandfluid.com/publicdownloads/AngularJSIn60MinutesIsh_DanWahlin_May2013.pdf>
* <http://iarouse.com/blog/2013/11/14/use-jquery-plugin-with-angularjs-the-easy-lazy-way/>
* <https://github.com/jmcunningham/AngularJS-Learning>
