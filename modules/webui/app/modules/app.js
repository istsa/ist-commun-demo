/*global define*/
define(['angular',
//        'togetherjs',
        'text!./app.html',
        'css!bootstrap-css',
        'css!styles/main',
        'css!font-awesome-css',
        'css!de-metr/style-metronic-minimized',
        'css!de-metr/style-minimized',
        'css!de-metr/default-minimized',
        'css!styles/library-style-overrides',
        'angular-route',
        'security/security',
        'home/home',
        'rlslog/rlslog',
        'help/help',
        'support/logs/logs',
        'angular-blocks'], function (angular, appTemplate) {
	
	'use strict';

  var module = angular.module('app', ['ngRoute', /*'$strap', */'angular-blocks', 'security', 'home', 'rlslog', 'help', 'logs']);

  module.run(['$templateCache', function($templateCache) {
    $templateCache.put('modules/app.html', appTemplate);
  }]);

  // Expecting ngmin to work on the following line:
	module.config(function ($httpProvider, $routeProvider) {

		var httpLogInterceptor;

		httpLogInterceptor = ['$q', function ($q) {

			function success(response) {
				console.log('Successful HTTP request. Response:', response);
				return response;
			}

			function error(response) {
				console.log('Error in HTTP request. Response:', response);
				return $q.reject(response);
			}

			return function (promise) {
				return promise.then(success, error);
			};
		}];

//		$httpProvider.responseInterceptors.push(httpLogInterceptor);
		
		$routeProvider.otherwise({
			redirectTo: '/'
		});
	});

  module.controller('PageCtrl', ['$scope', '$window', function($scope, $window) {

    $scope.$on('$routeChangeSuccess', function(event, currRoute/*, prevRoute*/) {
      $window.document.title = currRoute.title ? currRoute.title : 'MyProduct';
    });

  }]);

  module.controller('MenuCtrl', ['$scope', 'applicationMenu', 'userService', function($scope, applicationMenu, userService) {
    $scope.menu = applicationMenu;
    $scope.page = {
      title: '(page title)',
      subtitle: '(page subtitle)'
    };
    $scope.logout = function() {
      userService.logout();
    };
    $scope.$watch( function () { return userService.profile; }, function (userProfile) {
      $scope.userProfile = userProfile;
    }, true);
  }]);

  module.factory('applicationMenu', ['$location','$rootScope', function ($location, $rootScope) {

    var mainMenu = [
      {itemName: 'Σύστημα', implemented: true, subMenu: [
        {itemName: 'Ασφάλεια', implemented: true, subMenu: [
          {itemName: 'Χρήστες', location: 'users', implemented: true},
          {itemName: 'Ρόλοι', location: 'roleGroups', implemented: true},
          {itemName: 'Πολιτικές Συνθηματικού', location: 'passwordPolicies', implemented: true}
        ]},
        {itemName: 'Ιστορικό ενεργειών χρηστών', location: 'entityRevisions', implemented: true},
        {itemName: 'Ρυθμίσεις Συστήματος', location: 'logs', accessRole: 'logs_admin', implemented: true},
        {itemName: 'Στατιστικά Απόδοσης Συστήματος', implemented: false}
      ]}
    ];

    var applicationMenu = {
      items: mainMenu,
      selectItem: function(itemLocation) {
        function search(location, items) {
          for(var i=0; i<items.length; i++) {
            var item = items[i];
            if (item.location === location) {
              return i;
            } else if (item.subMenu !== undefined) {
              if (search(location, item.subMenu) !== -1) {
                return i;
              }
            }
          }
          return -1;
        }
        var index = search(itemLocation, mainMenu);
        if (index >= 0) {
          applicationMenu.selectedItem = mainMenu[index];
        }
      },
      navigate: function(item) {
        if (item.location) {
          applicationMenu.selectItem(item.location);
          $location.path('/'+item.location).search({});
//        } else if (item.action === 'TogetherJS') {
//          // XXX to satisfy jhint
//          var togetherJS = TogetherJS;
//          togetherJS();
        }
      },
      isAccessible: function(item, result) {
        if (item.subMenu) {
          for (var subItem in item.subMenu) {
            result = result || this.isAccessible(item.subMenu[subItem], result);
          }
          return result;
        } else {
          if (angular.isArray(item.accessRole)) {
            return $rootScope.hasAnyRole(item.accessRole);
          } else if (item.accessRole && !angular.isArray(item.accessRole)){
            return $rootScope.hasRole(item.accessRole);
          } else {
            return $rootScope.hasRole(item.location + '_viewer');
          }
        }
      }
    };

    applicationMenu.selectItem(mainMenu[0].itemName); // TODO: initialize from user profile

    return applicationMenu;
  }]);

  module.run(['$rootScope', '$location', 'applicationMenu', '$http', function ($rootScope, $location, applicationMenu, $http) {
	  $http.defaults.headers.common['Accept-Language'] = 'el, en;q=0.8'; // TODO: initialize from user profile

	  $rootScope.$on('$routeChangeSuccess', function(event, currRoute/*, prevRoute*/) {
      if (currRoute.originalPath === undefined) {
        return;
      }
      var location = currRoute.originalPath.replace(/^\/([^\/]*)(?:[\/].*)?$/i, '$1');
      applicationMenu.selectItem(location);
    });
  }]);

  return module;
});
