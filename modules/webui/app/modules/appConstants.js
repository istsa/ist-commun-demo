/*global define */
define([], function () {
  'use strict';

  return {
    appName: 'MyProduct',
    copyright: '(c) 2015-2016 MyCompany'
  };
});
