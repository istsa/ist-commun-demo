/*global define */
define(['angular',
  'text!./help.html',
  'angular-route',
  'support/youTubeLink/youTubeLink'
  ], function(angular, template) {

  'use strict';

  var module = angular.module('help', ['ngRoute', 'youTubeLink']);

  module.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
      .when('/help', {
        title: 'Βοήθεια - MyProduct',
        template: template
      });
  }]);

  return module;
});