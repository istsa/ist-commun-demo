/*global define */
define(['angular',
        'text!./home.html',
        'angular-route',
        'support/youTubeLink/youTubeLink'
        ], function(angular, template) {

  'use strict';

  var module = angular.module('home', ['ngRoute', 'youTubeLink']);

  module.config(['$routeProvider', function ($routeProvider) {
      $routeProvider
      .when('/', {
        title: 'Αρχική σελίδα - MyProduct',
        template: template
      });
    }]);

  return module;
});