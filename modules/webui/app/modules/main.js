/*global require*/
'use strict';
// Enable the following if you want to trace requirejs dependencies
//require(['onResourceLoad'], function () {
require(['../bower_components/ist-commun-webui/app/modules/requirejs-config', './requirejs-config'], function() {
  require(['angular', 'app'], function (angular) {
    angular.bootstrap(document, ['app']);
  });
});
//});
