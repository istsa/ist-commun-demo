/**
 * Traces dependencies. Uses requireJS internal API. See https://github.com/jrburke/requirejs/wiki/Internal-API%3a-onResourceLoad
 */
/*global requirejs */
requirejs.onResourceLoad = function (context, map, depArray) {
	'use strict';
	console.log('RJS', map.name+(map.parentMap ? '->'+map.parentMap.name : ''), JSON.stringify(depArray.map(function(el) {return el.name;})));
};