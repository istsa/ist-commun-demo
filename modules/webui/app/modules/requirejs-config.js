/*global require*/
'use strict';
require.config({
  paths: {
    'styles': '../styles',
    'font-awesome-css': '../bower_components/components-font-awesome/css/font-awesome',
//    'angular-strap': '../bower_components/angular-strap/dist/angular-strap',
    'entityCrud': '../bower_components/ist-commun-webui/app/modules/entityCrud',
    'security': '../bower_components/ist-commun-webui/app/modules/security',
    'support': '../bower_components/ist-commun-webui/app/modules/support',
    'core': '../bower_components/ist-commun-webui/app/modules/core',
    'appConstants': './appConstants'
  },
  shim: {
  },
  enforceDefine: false
});
