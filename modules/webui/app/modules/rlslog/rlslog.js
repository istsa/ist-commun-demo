/*global define */
define(['angular',
  'text!./rlslog.html',
  'angular-route'
  ], function(angular, template) {

  'use strict';

  var module = angular.module('rlslog', ['ngRoute']);

  module.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
      .when('/rlslog', {
        title: 'Ιστορικό εκδόσεων - MyProduct',
        template: template
      });
  }]);

  return module;
});