export MAVEN_OPTS="-Xmx1536m -Xss512k -XX:MaxPermSize=256m" 

cd modules/webapp

mvn jetty:run -Dspring.profiles.active="postgres,basic-auth,liquibase" -Dhibernate.hbm2ddl.auto=validate -Dliquibase.contexts=prod -Ddatabase.username=postgres -Ddatabase.password=postgres -Ddatabase.url=jdbc:postgresql://localhost:15432/postgres -Ddatabase.driverClassName=org.postgresql.Driver -Dliquibase.verbose=true -Dhibernate.default_schema=public

# mvn jetty:run -Dhibernate.hbm2ddl.auto=validate -Pliquibase -Dliquibase.contexts=prod -Ddatabase.username=postgres -Ddatabase.password=postgres -Ddatabase.url=jdbc:postgresql://localhost:15432/postgres -Ddatabase.driverClassName=org.postgresql.Driver -Dspring.profiles.active="postgres,basic-auth,liquibase" -Dliquibase.verbose=true -Dhibernate.default_schema=public
